# mask_mosaic
オブジェクトを指定してモザイクをかけるエフェクト

## Usage
1. mask_mosaic.xを読み込む
2. エフェクトファイル割り当てのMaskRTタブでモザイクをかけたいオブジェクトにtarget_obj.fxを割り当てる。


## Settings
* strength
  モザイクの荒さです。モザイクの作り方に手を抜いているせいでまともに使える値の範囲が狭いです。

* col_bias
  色付きのモザイクになればいいかなと思いました。単純に加算するだけなので値は少し決めづらい。

* spreadとSAMP_NUM
  spread*SAMP_NUMピクセルだけモザイクを適用する領域が広がります。  
  spread=0の場合その対象物体の範囲のみがモザイクとなります。  
  spreadは大きくしすぎると広がった領域がいびつになります。1.0か2.0推奨。  
  SAMP_NUMは大きくするとそれだけ重くなります。

* EXPAND_FLAG
  デフォルトは1でモザイクの範囲が広がります。
  モデルと背景を置いて背景だけにtarget_obj.fxを適用してから値を変えると効果がわかります。

* USE_CONTROLLER
  これを1にするとポストエフェクトのXファイルの
   * X値･･･strength
   * Z値･･･spread
   * Rxyz(回転)値･･･col_bias
  に対応します。

"#define"があるものは直接ファイルを編集してください。

## Agreement
* 用途
  * 一切の制限無し
* スクリプト
  * mask_mosaic.fx
    * MITライセンス
  * _none.fx/target_obj.fx
    * ベースとした舞力介入Pのfull.fx ver2.0に従う。
* 免責
  * どのような損害に対しても製作者は責任を負えませんので自己責任でご利用してください。

## Release
* 2015年3月18日 
  * 修正版
  * MMM対応
  * パラメータ追加

* 2015年3月10日
  * 初版


## Contact
 * https://twitter.com/GNX2_
 * https://gitlab.com/gnx2/
 * http://arithmeticoverflow.blog.fc2.com/blog-entry-76.html


## Author
**GNX2**
