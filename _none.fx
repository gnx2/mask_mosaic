////////////////////////////////////////////////////////////////////////////////////////////////
// ただの黒塗りエフェクト
//
// ベース
//   full.fx ver2.0
//   作成: 舞力介入P
//
////////////////////////////////////////////////////////////////////////////////////////////////
// パラメータ宣言
// 座法変換行列
float4x4 WorldViewProjMatrix      : WORLDVIEWPROJECTION;

#ifdef MIKUMIKUMOVING
float4x4 WorldMatrix    : WORLD;
float4x4 ViewMatrix     : VIEW;
float4x4 ProjMatrix     : PROJECTION;

float3   CameraPosition     : POSITION  < string Object = "Camera"; >;
#endif

// スクリーンサイズ
float2 ViewportSize : VIEWPORTPIXELSIZE;
static float2 ViewportOffset = float2(0.5,0.5)/ViewportSize;


technique EdgeTec < string MMDPass = "edge"; > { }
technique ShadowTec < string MMDPass = "shadow"; > { }

static const float4 black = {0.0,0.0,0.0,1.0};

///////////////////////////////////////////////////////////////////////////////////////////////
// オブジェクト描画

#ifdef MIKUMIKUMOVING
    #define VS_INPUT MMM_SKINNING_INPUT

#else
    struct VS_INPUT {
        float4 Pos  : POSITION;
    };

#endif

struct VS_OUTPUT {
    float4 Pos  : POSITION;
};



VS_OUTPUT Basic_VS(VS_INPUT IN)
{
    VS_OUTPUT Out = (VS_OUTPUT)0;

#ifdef MIKUMIKUMOVING
    MMM_SKINNING_OUTPUT SkinOut = MMM_SkinnedPositionNormal(IN.Pos, IN.Normal, IN.BlendWeight, IN.BlendIndices, IN.SdefC, IN.SdefR0, IN.SdefR1);
    float3 Eye = CameraPosition - mul( SkinOut.Position, WorldMatrix ).xyz;

    if (MMM_IsDinamicProjection){
        float4x4 wvpmat = mul(mul(WorldMatrix, ViewMatrix), MMM_DynamicFov(ProjMatrix, length(Eye)));
        Out.Pos = mul( SkinOut.Position, wvpmat );
    }else{
        Out.Pos = mul( SkinOut.Position, WorldViewProjMatrix );
    }

#else
    Out.Pos = mul( IN.Pos, WorldViewProjMatrix );

#endif

    return Out;
}

float4 Basic_PS(VS_OUTPUT IN) : COLOR0
{
    return black;
}


technique MainTec < string MMDPass = "object"; > {
    pass DrawObject {
        VertexShader = compile vs_2_0 Basic_VS();
        PixelShader  = compile ps_2_0 Basic_PS();
    }
}

#ifndef MIKUMIKUMOVING
technique MainTecBS  < string MMDPass = "object_ss"; > {
    pass DrawObject {
        VertexShader = compile vs_2_0 Basic_VS();
        PixelShader  = compile ps_2_0 Basic_PS();
    }
}
#endif

///////////////////////////////////////////////////////////////////////////////////////////////
